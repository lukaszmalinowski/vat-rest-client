package com.malinowski.vat.client.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ApiRoot {
    private String details;
    private String version;
    @JsonProperty("rates")
    private List<Country> countries;

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public List<Country> getCountries() {
        return countries;
    }

    public void setCountries(List<Country> countries) {
        this.countries = countries;
    }
}
