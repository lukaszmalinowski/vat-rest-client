package com.malinowski.vat.client;

import com.malinowski.vat.client.model.ApiRoot;
import com.malinowski.vat.client.model.Country;
import com.malinowski.vat.client.model.Period;
import com.malinowski.vat.report.model.CountryVatRate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class RestClientService implements ClientService {
    private static final Logger LOGGER = LoggerFactory.getLogger(RestClientService.class);
    static final String API_ADDRESS = "http://jsonvat.com/";

    private RestTemplate restTemplate;

    public RestClientService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public List<CountryVatRate> getHighestVatCountries(int amount) {
        return getVatCountries(amount, buildCountryVatRateComparator().reversed());
    }

    @Override
    public List<CountryVatRate> getLowestVatCountries(int amount) {
        return getVatCountries(amount, buildCountryVatRateComparator());
    }

    private Comparator<CountryVatRate> buildCountryVatRateComparator() {
        return Comparator.comparing(CountryVatRate::getRate)
                .thenComparing(CountryVatRate::getCountryName);
    }

    private List<CountryVatRate> getVatCountries(int amount, Comparator<CountryVatRate> comparator) {
        return getNewestCountryVatRates().stream()
                .sorted(comparator)
                .limit(amount)
                .collect(Collectors.toList());
    }

    private Set<CountryVatRate> getNewestCountryVatRates() {
        try {
            ApiRoot apiRoot = restTemplate.getForObject(API_ADDRESS, ApiRoot.class);
            return apiRoot.getCountries()
                    .stream()
                    .flatMap(this::countryVatRateMapper)
                    .collect(Collectors.toSet());
        } catch (RestClientException exception) {
            LOGGER.error("Cannot get response. It can be temporary problem with connection. Try again later.", exception);
            return Collections.emptySet();
        } catch (RuntimeException exception) {
            LOGGER.error("Cannot parse JSON. It can be caused by API changes.", exception);
            return Collections.emptySet();
        }
    }

    private Stream<CountryVatRate> countryVatRateMapper(Country country) {
        return country.getPeriods().stream()
                .max(Comparator.comparing(Period::getEffectiveFrom))
                .map(Period::getRates)
                .orElseGet(Collections::emptyMap)
                .entrySet()
                .stream()
                .map((entry) -> new CountryVatRate(country.getName(), entry.getValue()));
    }
}
