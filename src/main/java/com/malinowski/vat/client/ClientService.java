package com.malinowski.vat.client;

import com.malinowski.vat.report.model.CountryVatRate;

import java.util.List;

public interface ClientService {

    List<CountryVatRate> getHighestVatCountries(int amount);

    List<CountryVatRate> getLowestVatCountries(int amount);
}
