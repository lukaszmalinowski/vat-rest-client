package com.malinowski.vat;

import com.malinowski.vat.client.ClientService;
import com.malinowski.vat.report.ReportService;
import com.malinowski.vat.report.model.CountryVatRate;
import com.malinowski.vat.report.model.Report;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import java.util.List;

@SpringBootApplication
public class Application {

    private static final int NUMBER_OF_COUNTRIES = 3;
    private ReportService reportService;
    private ClientService clientService;

    public Application(ReportService reportService, ClientService clientService) {
        this.reportService = reportService;
        this.clientService = clientService;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void printReport() {
        List<CountryVatRate> lowestVatCountries = clientService.getLowestVatCountries(NUMBER_OF_COUNTRIES);
        List<CountryVatRate> highestVatCountries = clientService.getHighestVatCountries(NUMBER_OF_COUNTRIES);
        Report report = new Report(lowestVatCountries, highestVatCountries);
        reportService.print(report);
    }


    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
