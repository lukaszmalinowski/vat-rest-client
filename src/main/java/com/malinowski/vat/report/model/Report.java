package com.malinowski.vat.report.model;

import java.util.List;

public class Report {
    private final List<CountryVatRate> lowestVatCountries;
    private final List<CountryVatRate> highestVatCountries;

    public Report(List<CountryVatRate> lowestVatCountries, List<CountryVatRate> highestVatCountries) {
        this.lowestVatCountries = lowestVatCountries;
        this.highestVatCountries = highestVatCountries;
    }

    public List<CountryVatRate> getLowestVatCountries() {
        return lowestVatCountries;
    }

    public List<CountryVatRate> getHighestVatCountries() {
        return highestVatCountries;
    }
}
