package com.malinowski.vat.report.model;

import java.math.BigDecimal;
import java.util.Objects;

public class CountryVatRate {
    private final String countryName;
    private final BigDecimal rate;

    public CountryVatRate(String countryName, BigDecimal rate) {
        this.countryName = countryName;
        this.rate = rate;
    }

    public String getCountryName() {
        return countryName;
    }

    public BigDecimal getRate() {
        return rate;
    }

    @Override
    public String toString() {
        return countryName + " vat rate " + rate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CountryVatRate that = (CountryVatRate) o;
        return Objects.equals(countryName, that.countryName) &&
                Objects.equals(rate, that.rate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(countryName, rate);
    }
}
