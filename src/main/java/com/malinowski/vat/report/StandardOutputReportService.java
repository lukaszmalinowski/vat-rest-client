package com.malinowski.vat.report;

import com.malinowski.vat.report.model.Report;
import org.springframework.stereotype.Service;

@Service
public class StandardOutputReportService implements ReportService {

    private static final String LINE_SEPARATOR = "=========================";
    private static final String NEW_LINE = "\r\n";

    @Override
    public void print(Report report) {
        StringBuilder output = new StringBuilder()
                .append(LINE_SEPARATOR)
                .append(NEW_LINE)
                .append("Countries with lowest vat rates:")
                .append(NEW_LINE)
                .append(String.join(NEW_LINE, report.getLowestVatCountries().toString()))
                .append(NEW_LINE)
                .append("Countries with highest vat rates:")
                .append(NEW_LINE)
                .append(String.join(NEW_LINE, report.getHighestVatCountries().toString()))
                .append(NEW_LINE)
                .append(LINE_SEPARATOR);
        System.out.println(output.toString());
    }
}
