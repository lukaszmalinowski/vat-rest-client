package com.malinowski.vat.report;

import com.malinowski.vat.report.model.Report;

public interface ReportService {
    void print(Report report);
}
