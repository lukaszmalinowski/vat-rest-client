package com.malinowski.vat.client;

import com.malinowski.vat.ResourceUtils;
import com.malinowski.vat.report.model.CountryVatRate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withBadRequest;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@RunWith(SpringRunner.class)
@RestClientTest
@ContextConfiguration(classes = {ClientConfiguration.class, RestClientService.class})
public class RestClientServiceTest {

    @Autowired
    private ClientService clientService;

    @Autowired
    private MockRestServiceServer server;

    @Before
    public void setUp() throws Exception {
        String expectedOutput = ResourceUtils.loadFile("test/response.json");
        server.expect(requestTo(RestClientService.API_ADDRESS))
                .andRespond(withSuccess(expectedOutput, MediaType.APPLICATION_JSON));
    }

    @Test
    public void shouldReturnThreeHighestVatCountries() {
        List<CountryVatRate> highestVatCountries = clientService.getHighestVatCountries(3);
        assertEquals(3, highestVatCountries.size());
        assertEquals(Arrays.asList(new CountryVatRate("Hungary", new BigDecimal("27.0")),
                new CountryVatRate("Sweden", new BigDecimal("25.0")),
                new CountryVatRate("Denmark", new BigDecimal("25.0"))
        ), highestVatCountries);
    }

    @Test
    public void shouldReturnThreeLowestVatCountries() {
        List<CountryVatRate> lowestVatCountries = clientService.getLowestVatCountries(3);
        assertEquals(3, lowestVatCountries.size());
        assertEquals(Arrays.asList(new CountryVatRate("France", new BigDecimal("2.1")),
                new CountryVatRate("Luxembourg", new BigDecimal("3.0")),
                new CountryVatRate("Italy", new BigDecimal("4.0"))
        ), lowestVatCountries);
    }

    @Test
    public void shouldReturnEmptyListWhenRestServiceIsNotAvailable() {
        server.reset();
        server.expect(requestTo(RestClientService.API_ADDRESS))
                .andRespond(withBadRequest());
        List<CountryVatRate> lowestVatCountries = clientService.getLowestVatCountries(3);
        assertTrue(lowestVatCountries.isEmpty());
    }

    @Test
    public void shouldReturnEmptyListWhenJsonCannotBeParsed() {
        server.reset();
        server.expect(requestTo(RestClientService.API_ADDRESS))
                .andRespond(withSuccess("", MediaType.APPLICATION_JSON));
        List<CountryVatRate> lowestVatCountries = clientService.getLowestVatCountries(3);
        assertTrue(lowestVatCountries.isEmpty());
    }
}