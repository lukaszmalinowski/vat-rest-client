package com.malinowski.vat;

import com.malinowski.vat.client.ClientService;
import com.malinowski.vat.report.ReportService;
import com.malinowski.vat.report.model.CountryVatRate;
import com.malinowski.vat.report.model.Report;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ApplicationTest {
    private static final CountryVatRate SPAIN_VAT_RATE = new CountryVatRate("Spain", new BigDecimal("1"));

    @Mock
    private ReportService reportService;
    @Mock
    private ClientService clientService;
    @Captor
    private ArgumentCaptor<Report> reportCaptor;

    private Application application;


    @Before
    public void setUp() {
        application = new Application(reportService, clientService);
        when(clientService.getLowestVatCountries(3)).thenReturn(Collections.singletonList(SPAIN_VAT_RATE));
        when(clientService.getHighestVatCountries(3)).thenReturn(Collections.singletonList(SPAIN_VAT_RATE));
    }

    @Test
    public void shouldPrintReport() {
        List<CountryVatRate> expectedCountries = Collections.singletonList(SPAIN_VAT_RATE);

        application.printReport();

        verify(reportService).print(reportCaptor.capture());
        Report report = reportCaptor.getValue();
        assertEquals(expectedCountries, report.getLowestVatCountries());
        assertEquals(expectedCountries, report.getHighestVatCountries());
    }
}