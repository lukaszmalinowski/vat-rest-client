package com.malinowski.vat.report;

import com.malinowski.vat.ResourceUtils;
import com.malinowski.vat.report.model.CountryVatRate;
import com.malinowski.vat.report.model.Report;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class StandardOutputReportServiceTest {

    private static final CountryVatRate SPAIN_VAT_RATE = new CountryVatRate("Spain", new BigDecimal("1.0"));

    private StandardOutputReportService standardOutputReportService;

    @Before
    public void setUp() {
        standardOutputReportService = new StandardOutputReportService();
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowExceptionWhenReportIsNull() {
        standardOutputReportService.print(null);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowExceptionWhenReportDataAreInvalid() {
        Report report = new Report(null, null);
        standardOutputReportService.print(report);
    }

    @Test
    public void shouldPrintReportWhenCountriesAreProvided() throws Exception {
        String expectedOutput = ResourceUtils.loadFile("test/expectedStandardOutput.txt");
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        System.setOut(new PrintStream(output));
        List<CountryVatRate> countryVatRates = Collections.singletonList(SPAIN_VAT_RATE);
        Report report = new Report(countryVatRates, countryVatRates);
        standardOutputReportService.print(report);
        assertEquals(expectedOutput, output.toString());
    }
}