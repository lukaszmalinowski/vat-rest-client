package com.malinowski.vat;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ResourceUtils {
    public static String loadFile(String path) throws URISyntaxException, IOException {
        URL urlResource = ResourceUtils.class.getClassLoader().getResource(path);
        return new String(Files.readAllBytes(Paths.get(urlResource.toURI())));
    }
}
